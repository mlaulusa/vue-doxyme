import { createRouter, createWebHistory } from 'vue-router'
import type { RouteRecordRaw } from 'vue-router'
import SignIn from './components/SignIn.vue'
import SignUp from './components/SignUp.vue'
import Home from './components/Home.vue'
import Dashboard from './components/Dashboard.vue'
import userStore from './store/user'

const routes: Array<RouteRecordRaw> = [
    { path: '/sign-in', name: 'signin', component: SignIn },
    { path: '/sign-up', name: 'signup', component: SignUp },
    { path: '/', name: 'home', component: Home },
    { 
        path: '/dashboard', 
        name: 'dashboard', 
        component: Dashboard,
        meta: { 
            requiresAuth: true,
        }
    },
    {
        path: '/*',
        redirect: (to) => ({ name: 'home' })
    }
]
const history = createWebHistory()

const router = createRouter({
    history,
    routes,
})

router.beforeEach((to, from) => {
    if (to.meta.requiresAuth && !userStore.user) {
        return { name: 'signin' }
    }
})

export default router