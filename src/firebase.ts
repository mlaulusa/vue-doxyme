import { initializeApp } from 'firebase/app'

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyA5se36pYV86MjqniAC7hoKy-i1qumsxYQ",
  authDomain: "signin-41721.firebaseapp.com",
  projectId: "signin-41721",
  storageBucket: "signin-41721.appspot.com",
  messagingSenderId: "935077369739",
  appId: "1:935077369739:web:626c6ed44e0903adf8418e"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

export default app