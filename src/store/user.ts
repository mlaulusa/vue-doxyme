import { reactive, ref } from 'vue'
import type { UnwrapNestedRefs } from 'vue'
import app from '../firebase'
import { createUserWithEmailAndPassword, updateProfile, signInWithEmailAndPassword, getAuth, onAuthStateChanged, signOut } from 'firebase/auth'
import type { UserCredential, User } from 'firebase/auth'

interface State {
    loading: boolean,
    user: User | null,
    error: Error | null,
}

let store = reactive<State>({
    user: null,
    loading: false,
    error: null
})

onAuthStateChanged(getAuth(app), function(user: User | null) {
    if (user) {
        store.user = user
    } else {
        store.user = user
    }
})

function createUser(email: string, password: string, displayName?: string): Promise<void> {
    store.loading = true

    return createUserWithEmailAndPassword(getAuth(app), email, password)
        .then((data) => {
            store.user = data.user
            if (displayName) {
                return updateProfile(data.user, { displayName })
            }
        })
        .catch((err) => {
            store.error = err
        })
        .finally(() => {
            store.loading = false
        })
}

function signIn(email: string, password: string): Promise<void | UserCredential> {
    store.loading = true

    return signInWithEmailAndPassword(getAuth(app), email, password)
        .then((data) => {
            // store.user = data.user
        })
        .catch((err) => {
            store.error = err
        })
        .finally(() => {
            store.loading = false
        })
}

export function useUser(): User | null {
    return store.user
}

export function useSignInWithEmailAndPassword() {
    return {
        signInWithEmailAndPassword: signIn,
        error: store.error,
        loading: store.loading,
    }
}

export function useCreateUser() {
    return {
        createUserWithEmailAndPassword: createUser,
        error: store.error,
        loading: store.loading,
    }
}

export default store
