import { createApp } from 'vue'
import App from './App.vue'
import router from './Router'
import './assets/css/index.css'
import './firebase'

createApp(App)
    .use(router)
    .mount('#app')